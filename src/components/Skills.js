import React from "react";
import { Col, Row,} from "react-bootstrap";
import {
  DiJavascript1,
  DiReact,
  DiNodejs,
  DiMongodb,
  DiGit,
 
  DiBootstrap,
  DiCss3,
  DiHeroku,
  DiHtml5,
  DiDatabase

} from "react-icons/di";
import {
  SiSublimetext,
  SiGitlab,
  SiVercel,
  SiExpress,
  SiPostman
} from "react-icons/si";

function Techstack() {
  return (
    <>
    <h1 id="iconcolor" style={{textAlign: 'center', marginTop: '25px'}}> My Skills</h1>
    
        <Row style={{ justifyContent: "center", paddingBottom: "50px" }}>
     
      <Col id="centered-label">
      <div id="iconcolor" style={{textAlign: 'center',  marginTop: '25px'}}>
      <DiJavascript1 size="80px"/>
      <span id="iconcolor" style={{textAlign: 'center'}}>Javascript</span>
      </div>
      </Col>

      <Col id="centered-label">
      <div id="iconcolor" style={{textAlign: 'center',  marginTop: '25px'}}>
      <DiReact size="80px"/>
      <span id="iconcolor" style={{textAlign: 'center'}}>ReactJS</span>
      </div>
      </Col>

      <Col id="centered-label">
      <div id="iconcolor" style={{textAlign: 'center',  marginTop: '25px'}}>
      <DiNodejs size="80px"/>
      <span id="iconcolor" style={{textAlign: 'center'}}>NodeJS</span>
      </div>
      </Col>
      <Col id="centered-label">
      <div id="iconcolor" style={{textAlign: 'center',  marginTop: '25px'}}>
      <DiMongodb size="80px"/>
      <span id="iconcolor" style={{textAlign: 'center'}}>MongoDB</span>
      </div>
      </Col>

      <Col id="centered-label">
      <div id="iconcolor" style={{textAlign: 'center',  marginTop: '25px'}}>
      <SiSublimetext size="80px"/>
      <span id="iconcolor" style={{textAlign: 'center'}}>Sublime</span>
      </div>
      </Col>

      <Col id="centered-label">
      <div id="iconcolor" style={{textAlign: 'center',  marginTop: '25px'}}>
      <DiGit size="80px"/>
      <span id="iconcolor" style={{textAlign: 'center'}}>Git</span>
      </div>
      </Col>

      <Col id="centered-label">
      <div id="iconcolor" style={{textAlign: 'center',  marginTop: '25px'}}>
      <DiBootstrap size="80px"/>
      <span id="iconcolor" style={{textAlign: 'center'}}>Bootstrap</span>
      </div>
      </Col>

      <Col id="centered-label">
      <div id="iconcolor" style={{textAlign: 'center',  marginTop: '25px'}}>
      <DiCss3 size="80px"/>
      <span id="iconcolor" style={{textAlign: 'center'}}>Css</span>
      </div>
      </Col>

      <Col id="centered-label">
      <div id="iconcolor" style={{textAlign: 'center',  marginTop: '25px'}}>
      <DiHtml5 size="80px"/>
      <span id="iconcolor" style={{textAlign: 'center'}}>HTML</span>
      </div>
      </Col>

      <Col id="centered-label">
      <div id="iconcolor" style={{textAlign: 'center',  marginTop: '25px'}}>
      <SiExpress size="80px"/>
      <span id="iconcolor" style={{textAlign: 'center'}}>ExpressJS</span>
      </div>
      </Col>

      <Col id="centered-label">
      <div id="iconcolor" style={{textAlign: 'center',  marginTop: '25px'}}>
      <DiDatabase size="80px"/>
      <span id="iconcolor" style={{textAlign: 'center'}}>NoSQL</span>
      </div>
      </Col>

     
      
    </Row>

    <h1 id="iconcolor" style={{textAlign: 'center'}}> Tools</h1>
    <Row style={{ justifyContent: "center", paddingBottom: "50px" }}>

     <Col id="centered-label">
      <div id="iconcolor" style={{textAlign: 'center',  marginTop: '25px'}}>
      <DiHeroku size="80px"/>
      <span id="iconcolor" style={{textAlign: 'center'}}>Heroku</span>
      </div>
      </Col>
      <Col id="centered-label">
      <div id="iconcolor" style={{textAlign: 'center',  marginTop: '25px'}}>
      <SiPostman size="80px"/>
      <span id="iconcolor" style={{textAlign: 'center'}}>Postman</span>
      </div>
      </Col>
      <Col id="centered-label">
      <div id="iconcolor" style={{textAlign: 'center',  marginTop: '25px'}}>
      <SiVercel size="80px"/>
      <span id="iconcolor" style={{textAlign: 'center'}}>Vercel</span>
      </div>
      </Col>

      <Col id="centered-label">
      <div id="iconcolor" style={{textAlign: 'center',  marginTop: '25px'}}>
      <SiGitlab size="80px"/>
      <span id="iconcolor" style={{textAlign: 'center'}}>Gitlab</span>
      </div>
      </Col>



    </Row>
    </>
  );
}

export default Techstack;