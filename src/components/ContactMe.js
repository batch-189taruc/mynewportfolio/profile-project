import React from "react";
import { Col, Row,} from "react-bootstrap";

import {
  SiLinkedin,
  SiGmail
} from "react-icons/si";

function AboutMe() {


  return (
    <>
    <h6 id="iconcolor" style={{textAlign: 'center'}}> Please contact me thru the links below:</h6>
    
        <Row style={{ justifyContent: "center", paddingBottom: "50px" }}>

      <Col id="centered-label">
      <div id="iconcolor" style={{textAlign: 'center',  marginTop: '25px', cursor: "pointer"}}>
      <SiGmail onClick={() => window.location = 'mailto:taruc.jeromee@gmail.com'} size="80px"/>
      <span id="iconcolor" style={{textAlign: 'center', paddingLeft: '10px'}}>Send Email</span>
      </div>
      </Col>

      <Col id="centered-label">
      <div id="iconcolor" style={{textAlign: 'center',  marginTop: '25px', cursor: "pointer"}}>
      <SiLinkedin onClick={() => window.location = 'https://www.linkedin.com/in/jerome-taruc-aa4962110/'} size="80px"/>
      <span id="iconcolor" style={{textAlign: 'center', paddingLeft: '10px'}}>LinkedIn info</span>
      </div>
      </Col>


    </Row>
    </>
  );
}

export default AboutMe;